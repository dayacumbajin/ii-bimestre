package patterns

interface CarService {
    fun doService()
}

interface CarServiceDecorator : CarService

class BasicCarService : CarService {
    override fun doService() = println("\n" +"Realizando un chequeo básico ... HECHO")
}

class CarWash(private val carService: CarService) : CarServiceDecorator {
    override fun doService() {
        carService.doService()
        println("\n" +
                "Lavando coche ... HECHO")
    }
}

class InsideCarCleanup(private val carService: CarService) : CarServiceDecorator {
    override fun doService() {
        carService.doService()
        println("\n" +
                "Limpiando el coche por dentro ... HECHO")
    }
}

fun main(args: Array<String>) {
    val carService = InsideCarCleanup(CarWash(BasicCarService()))
    carService.doService()
}

